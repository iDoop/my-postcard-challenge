//
//  LoginError.swift
//  MyPostcardChallenge
//
//  Created by Dominic Opitz on 08.02.20.
//  Copyright © 2020 Dominic Opitz. All rights reserved.
//

import Foundation

struct LoginError: Codable {
    let success: Bool
    let errormessage: String
    let errortitle: String
    let errorcode: String
}
