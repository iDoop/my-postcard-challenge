//
//  LoginResponse.swift
//  MyPostcardChallenge
//
//  Created by Dominic Opitz on 06.02.20.
//  Copyright © 2020 Dominic Opitz. All rights reserved.
//

import Foundation

struct LoginResponse: Codable {
    let success: Bool
    let email: String
    let name: String
    let lastname: String
}
