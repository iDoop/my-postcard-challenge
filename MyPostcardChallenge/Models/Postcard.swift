//
//  Postcard.swift
//  MyPostcardChallenge
//
//  Created by Dominic Opitz on 09.02.20.
//  Copyright © 2020 Dominic Opitz. All rights reserved.
//

import Foundation

struct Postcard: Codable {
    enum Style: String, Codable {
        case regular, foldable
    }
    
    let identifier = UUID().uuidString
    var creation = Date()
    var style: Style = .regular
    var image: String?
    var frontText: String?
    var backText: String?
}
