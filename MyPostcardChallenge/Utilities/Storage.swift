//
//  Storage.swift
//  MyPostcardChallenge
//
//  Created by Dominic Opitz on 09.02.20.
//  Copyright © 2020 Dominic Opitz. All rights reserved.
//

import UIKit

/// Storage struct
public struct Storage {
    
    fileprivate init() { }
    
    public enum Directory: String {
        // Only documents and other data that is user-generated, or that cannot otherwise be recreated by your application, should be stored in the <Application_Home>/Documents directory and will be automatically backed up by iCloud.
        case documents
        
        // Only pictures that are user-generated, or cannot otherwise be recreated by your application, should be stored in the <Application_Home>/Documents/Pictures directory and will be automatically backed up by iCloud.
        case pictures = "Pictures"
        
        // Data that can be downloaded again or regenerated should be stored in the <Application_Home>/Library/Caches directory. Examples of files you should put in the Caches directory include database cache files and downloadable content, such as that used by magazine, newspaper, and map applications.
        case caches
    }
    
    /// Returns URL constructed from specified directory
    public static func getURL(for directory: Directory) -> URL {
        var searchPathDirectory: FileManager.SearchPathDirectory
        
        switch directory {
        case .documents, .pictures:
            searchPathDirectory = .documentDirectory
        case .caches:
            searchPathDirectory = .cachesDirectory
        }
        
        if var url = FileManager.default.urls(for: searchPathDirectory, in: .userDomainMask).first {
            if directory == .pictures {
                url.appendPathComponent(directory.rawValue, isDirectory: true)
            }
            
            return url
        } else {
            fatalError("Could not create URL for specified directory!")
        }
    }
    
    /// Returns URL constructed from specified directory with given file name
    public static func getURL(for directory: Directory, file name: String) -> URL? {
        guard fileExists(name, in: directory) else {
            return nil
        }
        
        return getURL(for: directory).appendingPathComponent(name, isDirectory: false)
    }
    
    /// Store an encodable struct to the specified directory on disk
    ///
    /// - Parameters:
    ///   - object: the encodable struct to store
    ///   - directory: where to store the struct
    ///   - fileName: what to name the file where the struct data will be stored
    public static func store<T: Encodable>(_ object: T, to directory: Directory, as fileName: String) {
        let url = getURL(for: directory).appendingPathComponent(fileName, isDirectory: false)
        
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(object)
            if FileManager.default.fileExists(atPath: url.path) {
                try FileManager.default.removeItem(at: url)
            }
            FileManager.default.createFile(atPath: url.path, contents: data, attributes: nil)
        } catch {
            fatalError(error.localizedDescription)
        }
    }
    
    /// Save list of Codable objects in documents directory under the given file name
    /// - Parameter file name: Name of the file including json or csv file type ending
    /// - Parameter list: Codable objects which  get stored
    public static func store<T: Encodable>(_ fileName: String, list: [T]) {
        Storage.store(list, to: .documents, as: fileName)
    }
    
    /// Save image in pictures directory with the given file name
    /// - Parameter file name: Name of the file including jpeg or png file type ending
    /// - Parameter list: Image data with matching files names type which  get stored
    public static func store(_ fileName: String, image: Data) {
        do  {
            var url = Storage.getURL(for: .pictures)
            var isDirectory: ObjCBool = true
            if !FileManager.default.fileExists(atPath: url.path, isDirectory: &isDirectory) {
                try FileManager.default.createDirectory(at: url, withIntermediateDirectories: false, attributes: nil)
            }
            url = url.appendingPathComponent(fileName)
            if FileManager.default.fileExists(atPath: url.path) {
                try FileManager.default.removeItem(at: url)
            }
            try image.write(to: url, options: .atomic)
        } catch let err {
            print("Saving file resulted in error: ", err)
        }
    }
    
    /// Retrieve and convert a struct from a file on disk
    ///
    /// - Parameters:
    ///   - fileName: name of the file where struct data is stored
    ///   - directory: directory where struct data is stored
    ///   - type: struct type (i.e. Message.self)
    /// - Returns: decoded struct model(s) of data
    public static func retrieve<T: Decodable>(_ fileName: String, from directory: Directory, as type: T.Type) -> T {
        let url = getURL(for: directory).appendingPathComponent(fileName, isDirectory: false)
        
        if !FileManager.default.fileExists(atPath: url.path) {
            fatalError("File at path \(url.path) does not exist!")
        }
        
        if let data = FileManager.default.contents(atPath: url.path) {
            let decoder = JSONDecoder()
            do {
                let model = try decoder.decode(type, from: data)
                return model
            } catch {
                fatalError(error.localizedDescription)
            }
        } else {
            fatalError("No data at \(url.path)!")
        }
    }
    
    /// Load list of Codable objects from documents directory from given file name
    /// - Parameter file: Name of the file including json or csv file type ending
    public static func retrieve<T: Codable>(_ fileName: String) -> [T] {
        guard Storage.fileExists(fileName, in: .documents) else {
            return []
        }
        
        return Storage.retrieve(fileName, from: .documents, as: [T].self)
    }
    
    /// Load stored image from given file name
    /// - Parameter image: Name of image file
    public static func retrieve(image: String) -> UIImage? {
        let url = getURL(for: .pictures).appendingPathComponent(image, isDirectory: false)
        guard let fileData = FileManager.default.contents(atPath: url.path) else {
            return nil
        }
        
        return UIImage(data: fileData)
    }
    
    /// Remove specified file from specified directory
    public static func remove(_ fileName: String, from directory: Directory) {
        let url = getURL(for: directory).appendingPathComponent(fileName, isDirectory: false)
        if FileManager.default.fileExists(atPath: url.path) {
            do {
                try FileManager.default.removeItem(at: url)
            } catch {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    /// Remove specified file from specified url
    public static func remove(_ fileURL: URL) {
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(at: fileURL)
            } catch {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    /// Returns BOOL indicating whether file exists at specified directory with specified file name
    public static func fileExists(_ fileName: String, in directory: Directory) -> Bool {
        let url = getURL(for: directory).appendingPathComponent(fileName, isDirectory: false)
        return FileManager.default.fileExists(atPath: url.path)
    }
}
