//
//  Extensions.swift
//  MyPostcardChallenge
//
//  Created by Dominic Opitz on 10.02.20.
//  Copyright © 2020 Dominic Opitz. All rights reserved.
//

import Foundation

extension String {
    var wordCount: Int {
        let components = self.components(separatedBy: .whitespacesAndNewlines)
        let words = components.filter { !$0.isEmpty }
        
        return words.count
    }
}

extension Date {
    var formatted: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        
        return formatter.string(from: self)
    }
}

extension Postcard.Style {
    var title: String {
        return rawValue.capitalized
    }
    
    var hasBackText: Bool {
        // Only foldable style has back text
        return self == .foldable
    }
}
