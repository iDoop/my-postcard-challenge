//
//  PostcardArchiveTableViewCell.swift
//  MyPostcardChallenge
//
//  Created by Dominic Opitz on 11.02.20.
//  Copyright © 2020 Dominic Opitz. All rights reserved.
//

import UIKit

class PostcardArchiveTableViewCell: UITableViewCell {
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var styleLabel: UILabel!
    @IBOutlet weak var frontTextLabel: UILabel!
    @IBOutlet weak var backTextLabel: UILabel!
    
    func update(image: UIImage?, and postcard: Postcard) {
        previewImageView.image = image
        styleLabel.text = postcard.style.title
        frontTextLabel.text = postcard.frontText
        backTextLabel.text = postcard.backText
        backTextLabel.isHidden = !postcard.style.hasBackText
    }
}
