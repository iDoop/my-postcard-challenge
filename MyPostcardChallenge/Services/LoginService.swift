//
//  LoginService.swift
//  MyPostcardChallenge
//
//  Created by Dominic Opitz on 06.02.20.
//  Copyright © 2020 Dominic Opitz. All rights reserved.
//

import Foundation

class LoginService {
    
    private struct Constants {
        static let requestURL = "https://www.mypostcard.com/mobile/login.php?json=1"
        static let requestMethod = "POST"
        static let requestContentType = "application/x-www-form-urlencoded"
        static let requestContentTypeHeader = "Content-Type"
        static let requestParamEmail = "email"
        static let requestParamPassword = "password"
    }
    
    static let shared = LoginService()
    
    func login(email: String, password: String, _ completion: @escaping (LoginResponse?, LoginError?) -> Void) {
        let body = "\(Constants.requestParamEmail)=\(email)&\(Constants.requestParamPassword)=\(password)"
        var request = URLRequest(url: URL(string: Constants.requestURL)!)
        request.httpMethod = Constants.requestMethod
        request.setValue(Constants.requestContentType, forHTTPHeaderField: Constants.requestContentTypeHeader)
        request.httpBody = body.data(using:String.Encoding.ascii, allowLossyConversion: false)

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    print("Login error: \(error)")
                    completion(nil, nil)
                    return
                }
                
                guard let data = data else {
                    print("Login failed with invalid response data")
                    completion(nil, nil)
                    return
                }
                
                print("Login data: \(String(data: data, encoding: .utf8) ?? "")")
                let loginResponse = try? JSONDecoder().decode(LoginResponse.self, from: data)
                let loginError = try? JSONDecoder().decode(LoginError.self, from: data)
                completion(loginResponse, loginError)
            }
        }
        task.resume()
    }
}
