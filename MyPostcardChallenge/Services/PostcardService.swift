//
//  PostcardService.swift
//  MyPostcardChallenge
//
//  Created by Dominic Opitz on 09.02.20.
//  Copyright © 2020 Dominic Opitz. All rights reserved.
//

import UIKit

class PostcardService {
    
    private struct Constants {
        static let jpegCompression: CGFloat = 0.8
        static let jpegFileExtension = "jpg"
        static let archiveFileName = "postcard-archive.json"
        static let frontTextCharacterLimit = 30
        static let backTextWordLimit = 30
    }
    
    static let shared = PostcardService()
    
    private(set) var creation = Postcard()
    private(set) var archive: [Postcard] = Storage.retrieve(Constants.archiveFileName)
    
    var frontTextCharacterLimit: Int {
        return Constants.frontTextCharacterLimit
    }
    
    var backTextWordLimit: Int {
        return Constants.backTextWordLimit
    }
    
    func set(creation postcard: Postcard) {
        creation = postcard
    }
    
    func set(style: Postcard.Style) {
        creation.style = style
    }
    
    func set(image: UIImage) {
        guard let data = image.jpegData(compressionQuality: Constants.jpegCompression) else {
            return
        }
        
        let fileName = "\(creation.identifier).\(Constants.jpegFileExtension)"
        Storage.store(fileName, image: data)
        creation.image = fileName
    }
    
    func set(frontText: String?, backText: String?) {
        creation.frontText = frontText
        creation.backText = backText
    }
    
    func create() -> Bool {
        let saved = save(postcard: creation)
        if saved {
            creation = Postcard()
        }
        
        return saved
    }
    
    func save(postcard: Postcard) -> Bool {
        guard postcard.image != nil, !(postcard.frontText ?? "").isEmpty else {
            return false
        }
        
        var postcard = postcard
        // update creation date
        postcard.creation = Date()
        if let index = archive.firstIndex(where: {$0.identifier == postcard.identifier}) {
            archive[index] = postcard
        } else {
            archive.append(postcard)
        }
        
        Storage.store(Constants.archiveFileName, list: archive)
        
        return true
    }
    
    func remove(postcard: Postcard) -> Bool {
        guard let index = archive.firstIndex(where: {$0.identifier == postcard.identifier}) else {
            return false
        }
        
        archive.remove(at: index)
        Storage.store(Constants.archiveFileName, list: archive)
        
        return true
    }
    
    func image(for postcard: Postcard) -> UIImage? {
        guard let image = postcard.image else {
            return nil
        }
        
        return Storage.retrieve(image: image)
    }
}
