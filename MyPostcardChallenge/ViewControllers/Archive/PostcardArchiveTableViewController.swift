//
//  PostcardArchiveTableViewController.swift
//  MyPostcardChallenge
//
//  Created by Dominic Opitz on 11.02.20.
//  Copyright © 2020 Dominic Opitz. All rights reserved.
//

import UIKit

class PostcardArchiveTableViewController: UITableViewController {

    private struct Constants {
        static let postcardCellIdentifier = "postcardCell"
        static let creationSegueIdentifier = "creationSegue"
        static let filterOptionsAlertTitle = "Filter postcard style"
        static let filterOptionsAll = "All"
        static let selectionOptionLoad = "Load a card"
        static let selectionOptionSync = "Sync"
        static let selectionOptionDelete = "Delete"
        static let syncAlertTitle = "Sync succeeded"
        static let loadingErrorAlertTitle = "Loading did fail"
        static let deletionErrorAlertTitle = "Deletion did fail"
        static let alertOptionCancel = "Cancel"
        static let alertOptionOK = "OK"
    }
    
    private var archive: [Postcard] = []
    private var filter: Postcard.Style?
    
    private func fetchArchive(filter style: Postcard.Style? = nil) {
        archive.removeAll()

        if let style = style {
            archive.append(contentsOf: PostcardService.shared.archive.filter({$0.style == style}))
        } else {
            archive.append(contentsOf: PostcardService.shared.archive)
        }
        
        // sort by date
        archive.sort(by: {$0.creation > $1.creation })
        filter = style
        let range = NSMakeRange(0, tableView.numberOfSections)
        let sections = NSIndexSet(indexesIn: range)
        tableView.reloadSections(sections as IndexSet, with: .automatic)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fetchArchive(filter: filter)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return archive.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: Constants.postcardCellIdentifier, for: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? PostcardArchiveTableViewCell else {
            return
        }
        
        let postcard = archive[indexPath.row]
        cell.update(image: PostcardService.shared.image(for: postcard), and: postcard)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else {
            return
        }
            
        deletePostcard(at: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectPostcard(at: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let creationController = segue.destination as? PostcardCreationTabBarController {
            creationController.preSelectedTab = .preview
        }
    }
    
    @IBAction func filterButtonTapped(_ sender: Any) {
        filterPostcards()
    }
    
    private func deletePostcard(at indexPath: IndexPath) {
        let postcard = archive[indexPath.row]
        if PostcardService.shared.remove(postcard: postcard) {
            archive.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        } else {
            showAlert(title: Constants.deletionErrorAlertTitle, message: nil)
        }
    }
    
    private func selectPostcard(at indexPath: IndexPath) {
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let loadAction = UIAlertAction(title: Constants.selectionOptionLoad, style: .default) { (action) in
            // fail loading every second postcard if sync is not available
            if self.load(row: indexPath.row) {
                let postcard = self.archive[indexPath.row]
                PostcardService.shared.set(creation: postcard)
                self.performSegue(withIdentifier: Constants.creationSegueIdentifier, sender: self)
            } else {
                self.showAlert(title: Constants.loadingErrorAlertTitle, message: nil)
            }
        }
        
        let syncAction = UIAlertAction(title: Constants.selectionOptionSync, style: .default) { (action) in
            self.showAlert(title: Constants.syncAlertTitle, message: nil)
        }
        
        let deleteAction = UIAlertAction(title: Constants.selectionOptionDelete, style: .destructive) { (action) in
            self.deletePostcard(at: indexPath)
        }
        
        let cancelAction = UIAlertAction(title: Constants.alertOptionCancel, style: .cancel, handler: nil)

        optionMenu.addAction(loadAction)
        if sync(row: indexPath.row) {
            optionMenu.addAction(syncAction)
        }
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(cancelAction)
        
        present(optionMenu, animated: true, completion: nil)
    }
    
    private func filterPostcards() {
        let optionMenu = UIAlertController(title: Constants.filterOptionsAlertTitle,
                                           message: nil, preferredStyle: .actionSheet)
        
        let noFilterAction = UIAlertAction(title: Constants.filterOptionsAll, style: .default) { (action) in
            self.fetchArchive()
        }
        
        let regularFilterAction = UIAlertAction(title: Postcard.Style.regular.title, style: .default) { (action) in
            self.fetchArchive(filter: .regular)
        }
        
        let foldableFilterAction = UIAlertAction(title: Postcard.Style.foldable.title, style: .default) { (action) in
            self.fetchArchive(filter: .foldable)
        }
        
        let cancelAction = UIAlertAction(title: Constants.alertOptionCancel, style: .cancel, handler: nil)

        optionMenu.addAction(noFilterAction)
        optionMenu.addAction(regularFilterAction)
        optionMenu.addAction(foldableFilterAction)
        optionMenu.addAction(cancelAction)
        
        present(optionMenu, animated: true, completion: nil)
    }
    
    private func showAlert(title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: Constants.alertOptionOK, style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    private func sync(row: Int) -> Bool {
        // sync only visible in every 3rd row
        return (row + 1) % 3 == 0
    }
    
    private func load(row: Int) -> Bool {
        // load fails every 2nd row when sync is not available
        return (row + 1) % 2 != 0 || sync(row: row)
    }
}
