//
//  LoginViewController.swift
//  MyPostcardChallenge
//
//  Created by Dominic Opitz on 06.02.20.
//  Copyright © 2020 Dominic Opitz. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    private struct Constants {
        static let mainViewControllerSegue = "mainViewControllerSegue"
        static let errorTitle = "Login failed"
        static let errorMessage = "Something went wrong. Please try again."
        static let errorActionTitle = "OK"
    }
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        guard let email = emailTextField.text, let password = passwordTextField.text else {
            showErrorAlert()
            return
        }
        
        activityIndicator.startAnimating()
        loginButton.isEnabled = false
        
        LoginService.shared.login(email: email, password: password) { (response, error) in
            self.activityIndicator.stopAnimating()
            self.loginButton.isEnabled = true
            
            if response?.success == true {
                self.performSegue(withIdentifier: Constants.mainViewControllerSegue, sender: self)
            } else {
                self.showErrorAlert(title: error?.errortitle, message: error?.errormessage)
            }
        }
    }
    
    private func showErrorAlert(title: String? = nil, message: String? = nil) {
        let title = title ?? Constants.errorTitle
        let message = message ?? Constants.errorMessage
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Constants.errorActionTitle, style: .cancel, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
}
