//
//  MainViewController.swift
//  MyPostcardChallenge
//
//  Created by Dominic Opitz on 08.02.20.
//  Copyright © 2020 Dominic Opitz. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    private struct Constants {
        static let archiveButtonTitleFormat = "Archive (%d)"
    }
    
    @IBOutlet weak var archiveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.setHidesBackButton(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        PostcardService.shared.set(creation: Postcard())
        archiveButton.setTitle(String(format: Constants.archiveButtonTitleFormat, PostcardService.shared.archive.count),
                               for: .normal)
    }
}
