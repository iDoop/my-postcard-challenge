//
//  PostcardStyleViewController.swift
//  MyPostcardChallenge
//
//  Created by Dominic Opitz on 09.02.20.
//  Copyright © 2020 Dominic Opitz. All rights reserved.
//

import UIKit

class PostcardStyleViewController: UIViewController {
    
    @IBOutlet weak var styleSegmentedControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch PostcardService.shared.creation.style {
        case .regular:
            styleSegmentedControl.selectedSegmentIndex = 0
        case .foldable:
            styleSegmentedControl.selectedSegmentIndex = 1
        }
    }
    
    @IBAction func styleDidChange(_ sender: Any) {
        let style: Postcard.Style = styleSegmentedControl.selectedSegmentIndex == 0 ? .regular : .foldable
        PostcardService.shared.set(style: style)
    }
}
