//
//  PostcardSummaryViewController.swift
//  MyPostcardChallenge
//
//  Created by Dominic Opitz on 09.02.20.
//  Copyright © 2020 Dominic Opitz. All rights reserved.
//

import UIKit

class PostcardSummaryViewController: UIViewController {
    
    private struct Constants {
        static let mainViewControllerSegue = "mainViewControllerSegue"
        static let saveSuccessTitle = "Saved postcard"
        static let saveSuccessMessage = "Postcard successfully saved to archive."
        static let saveErrorTitle = "Failed saving postcard"
        static let saveErrorMessage = "Postcard has missing data and can't be saved."
        static let actionTitle = "OK"
    }
    
    @IBOutlet weak var styleLabel: UILabel!
    @IBOutlet weak var frontTextLabel: UILabel!
    @IBOutlet weak var backTextLabel: UILabel!
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var backTextStackView: UIStackView!
    @IBOutlet weak var creationDateLabel: UILabel!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // TODO: Move to viewWillAppear and make later updates with delegate in PostcardService which gets called when something has changed
        updateSummary()
    }
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        if PostcardService.shared.create() {
            navigationController?.popViewController(animated: true)
        } else {
            // invalid data
            showAlert(title: Constants.saveErrorTitle, message: Constants.saveErrorMessage)
        }
    }
    
    private func updateSummary() {
        let postcard = PostcardService.shared.creation
        styleLabel.text = postcard.style.title
        frontTextLabel.text = postcard.frontText
        backTextLabel.text = postcard.backText
        backTextStackView.isHidden = !postcard.style.hasBackText
        creationDateLabel.text = postcard.creation.formatted
        previewImageView.image = PostcardService.shared.image(for: postcard)
    }
    
    private func showAlert(title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Constants.actionTitle, style: .cancel, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
}
