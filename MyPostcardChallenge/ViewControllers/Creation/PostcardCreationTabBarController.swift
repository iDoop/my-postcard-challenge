//
//  PostcardCreationTabBarController.swift
//  MyPostcardChallenge
//
//  Created by Dominic Opitz on 09.02.20.
//  Copyright © 2020 Dominic Opitz. All rights reserved.
//

import UIKit

class PostcardCreationTabBarController: UITabBarController {
    
    enum Tab: Int {
        case style = 0
        case image = 1
        case text = 2
        case preview = 3
        case summary = 4
    }
    
    var preSelectedTab: Tab?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let tab = preSelectedTab {
            selectedViewController = viewControllers?[tab.rawValue]
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        title = tabBar.selectedItem?.title
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        title = item.title
    }
}
