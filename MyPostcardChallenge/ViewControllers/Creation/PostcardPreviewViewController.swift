//
//  PostcardPreviewViewController.swift
//  MyPostcardChallenge
//
//  Created by Dominic Opitz on 09.02.20.
//  Copyright © 2020 Dominic Opitz. All rights reserved.
//

import UIKit

class PostcardPreviewViewController: UIViewController {
    
    @IBOutlet weak var previewImageView: UIImageView!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let postcard = PostcardService.shared.creation
        let image = PostcardService.shared.image(for: postcard)
        previewImageView.image = image
    }
}
