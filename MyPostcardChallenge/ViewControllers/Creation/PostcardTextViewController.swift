//
//  PostcardTextViewController.swift
//  MyPostcardChallenge
//
//  Created by Dominic Opitz on 09.02.20.
//  Copyright © 2020 Dominic Opitz. All rights reserved.
//

import UIKit

class PostcardTextViewController: UIViewController {
    
    @IBOutlet weak var frontTextField: UITextField!
    @IBOutlet weak var backTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        frontTextField.delegate = self
        backTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        updateTexts()
    }
    
    private func updateTexts() {
        let postcard = PostcardService.shared.creation
        frontTextField.text = postcard.frontText
        backTextField.text = postcard.backText
        backTextField.isHidden = !postcard.style.hasBackText
    }
    
    private func saveTexts() {
        let frontText = frontTextField.text
        let backText = backTextField.text
        PostcardService.shared.set(frontText: frontText, backText: backText)
    }
}

extension PostcardTextViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        saveTexts()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text, !string.isEmpty else {
            return true
        }
        
        switch textField {
        case frontTextField:
            // character limit
            return text.count < PostcardService.shared.frontTextCharacterLimit
        case backTextField:
            // word limit
            return text.wordCount < PostcardService.shared.backTextWordLimit || string != " "
        default:
            return true
        }
    }
        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return false
    }    
}
